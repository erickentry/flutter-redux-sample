import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Store<AppState> store = Store<AppState>(appReducer,
      initialState: AppState.initial(), middleware: middlewares());

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

List<Middleware<AppState>> middlewares() => [
      TypedMiddleware<AppState, AddCounterAction>(_simulateRemoteCall),
    ];

Future _simulateRemoteCall(
    Store<AppState> store, AddCounterAction action, NextDispatcher next) async {
  await Future.sync(() => sleep(Duration(seconds: 1)));
  next(action);
}

AppState appReducer(AppState state, action) =>
    AppState(counterReducer(state.counter, action));

final Reducer<int> counterReducer =
    TypedReducer<int, AddCounterAction>(_addCounter);

int _addCounter(int counter, AddCounterAction action) =>
    counter + action.additive;

class AppState {
  int counter;

  AppState(this.counter);

  factory AppState.initial() => AppState(0);
}

class AddCounterAction {
  final int additive;

  AddCounterAction(this.additive);
}

class _ViewModel {
  final String pageTitle;
  final int counter;
  final Function onAddCounter;

  _ViewModel(this.pageTitle, this.counter, this.onAddCounter);

  factory _ViewModel.create(Store<AppState> store) {
    return _ViewModel("Hello Redux", store.state.counter,
        () => store.dispatch(AddCounterAction(1)));
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => Scaffold(
            appBar: AppBar(
              title: Text(viewModel.pageTitle),
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'You have pushed the button this many times:',
                  ),
                  Text(
                    '${viewModel.counter}',
                    style: Theme.of(context).textTheme.display1,
                  ),
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: viewModel.onAddCounter,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
          ),
    );
  }
}
